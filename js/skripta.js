$(document).ready(function() {
    var baseUrl = "https://chat.data-lab.si/api";
    var user = {id: 33, name: "Mojca Marjetica"}; //TODO: vnesi svoje podatke
    var nextMessageId = 0;
    var currentRoom = "Skedenj";

    //Naloži seznam sob
    $.ajax({
        url: baseUrl + "/rooms",
        type: "GET",
        success: function (data) {
            for (i in data) {
                $("#rooms").append(" \
                    <li class='media'> \
                        <div class='media-body room' style='cursor: pointer;'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                                </a> \
                                <div class='media-body'> \
                                    <h5>" + data[i] + "</h5> \
                                </div> \
                            </div> \
                        </div> \
                    </li>");
            }
        }
    });

    //TODO: Naloga
    //Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
    var pridobivanjePogovor = function(){
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
            type: 'GET',
            success: function(data){
                for(i in data){
                    var message = data[i];
                   $("#messages").append("<li class='media'> \
                            <div class='media-body'> \
                                <div class='media'> \
                                    <a class='pull-left' href='#'> \
                                        <img class='media-object img-circle ' src='https://randomuser.me/api/portraits/men/42.jpg' /> \
                                    </a> \
                                    <div class='media-body' > \
                                        <small class='text-muted'>Jhon Rexa | 2017-05-19 11:13:25</small> <br /> \
                                        Ojla, kako si? \
                                        <hr /> \
                                    </div> \
                                </div> \
                            </div> \
                        </li>");
                }
                
            }
        })
    }
    
    //Klicanje funkcije za začetek nalaganja pogovorov
    
    //TODO: Naloga
    //Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
    
    //Klicanje funkcije za začetek posodabljanja uporabnikov





    
    //Definicija funkcije za pošiljanje sporočila
    var posljiSporocilo = function(){
        var sporocilo = document.getElementById("message").value;
        console.log(sporocilo)
        
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
            type: "POST",
            success: function(data){
                $("#messages").append(" \
                    <li class='media'> \
                        <div class='media-body'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + user.id + ".jpg' /> \
                                </a> \
                                <div class='media-body' > \
                                        <small class='text-muted'>Alex Deo | 2017-05-19 11:12:35</small> <br /> \
                                        Živijo! \
                                        <hr /> \
                                    </div> \
                            </div> \
                        </div> \
                    </li>");
            }
        })
    }
    
    //On Click handler za pošiljanje sporočila
    
    document.getElementById("send").addEventListener('click', posljiSporocilo);









    //TODO: Naloga
    //Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
    //V razmislek: pomisli o morebitnih težavah!

    //On Click handler za menjavo sobe
    //Namig: Seznam sob se lahko naloži kasneje, kot koda, ki se izvede tu.
});